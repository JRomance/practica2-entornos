package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) an1alizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter='a';
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);// El error esta aqui.
		/*El error esta aqui por que el valor introducido en la variable char en la linea 13  tenia el numero 27 que lo que en codigo ascci representa
		 * al escape. El metodo indexof te devuelve la posici�n que un caracter y con substring creas una nueva cadena quitando el caracter.
		 * la soluci�n es poner o en numero ascci un valor conocido o un caracter que desees*/
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
