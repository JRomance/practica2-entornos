package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i <= cadenaLeida.length()-1; i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	/*
	 * El error ocurre en la l�nea 27 porque  cuando la variable cadenaLeida a trav�s de la funci�n .length() le indicamos el numero de veces que tiene que recorrer entonces le devuelve el tama�o de la cadena. En el caso de a1 le devolver�a el valor 2. El bucle for recorre desde 0 hasta el tama�o cadenaLeida.length() en nuestro caso 2
					0	1	2
					A	1	�
		Cuando la funci�n charAt(i)  recorre i=0, enuentra una A y suma a 1 en cantidad de vocales con i=1 encontrara el 1 pero cuando busca el valor 2 no hay ning�n valor en la cadena y salta una excepci�n de que se ha pasado de tama�o.
		Para solucionarlo podemos restarle uno valor al tama�o que tiene que recorrer nuestro bucle.
		for(int i = 0 ; i <= cadenaLeida.length()-1; i++)

	 * */
	}

}
