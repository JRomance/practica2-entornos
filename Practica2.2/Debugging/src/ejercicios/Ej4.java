package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i > 0  ; i--){//el error esta aqui ya que al dividir el bucle no puede llegar a cero
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}
/*la solucion es dentro del for donde realizamos las divisiones en el rango hasta donde tiene que llegar eliminar el igual*/
}
