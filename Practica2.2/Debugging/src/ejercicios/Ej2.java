package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		input.nextLine();
		System.out.println("Introduce un numero como String");
		//Da error en la linea 23. al coger el enter anterior
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		/*Como se puede ver en la primera captura de pantalla la cadenaLeida como String coje el enter de introducir el numero entero que se queda guardado en el buffer 
		 * y se produce la entrada de ese espacio como el segundo numero.
		 * La soluci�n es tan f�cil como limpiar el buffer con un input.nextLine()Tal como se indica en la linea 20
		 * As� consigues que no te coja el espacio y puedas introducir el numero y compare con el valor correcto.
*/
		
	}
}
