package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JTable;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import java.awt.TextField;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTree;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JProgressBar;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JMenu;

public class Ventana extends JFrame {
/**
 * @author JesusRomance
 * @since 31\01\2018
 * */
	private JPanel contentPane;
	private JMenuBar menuBar;
	private final JCheckBox Trabajador = new JCheckBox("Trabajador");
	private final JCheckBox Socio = new JCheckBox("Socio");
	private final JCheckBox Invitado = new JCheckBox("Invitado");
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtApellido_1;
	private JTextField txtDni;
	private JTextField txtEmail;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField;
	private JLabel lblNewLabel;
	private JMenuBar menuBar_1;
	private JMenu mnPersonal;
	private JMenu mnInvitado;
	private JMenu mnSocio;
	private JMenu mnTrabajador;
	private JMenu menu;
	private JMenu menu_1;
	private JMenu menu_2;
	private JMenu menu_3;
	private JMenu menu_4;
	private JMenu menu_5;
	private JMenu menu_6;
	private JMenu menu_7;
	private JMenu menu_8;
	private JMenu menu_9;
	private JMenu menu_10;
	private JMenu menu_11;
	private JMenu menu_12;
	private JMenu menu_13;
	private JMenu menu_14;
	private JMenu menu_15;
	private JMenu menu_16;
	private JMenu menu_17;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 465);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuBar_1 = new JMenuBar();
		menuBar.add(menuBar_1);
		
		mnPersonal = new JMenu("Personal");
		menuBar.add(mnPersonal);
		
		mnInvitado = new JMenu("Invitado");
		mnPersonal.add(mnInvitado);
		
		mnSocio = new JMenu("Socio");
		mnPersonal.add(mnSocio);
		
		mnTrabajador = new JMenu("Trabajador");
		mnPersonal.add(mnTrabajador);
		
		menu = new JMenu("Juego");
		menuBar.add(menu);
		
		menu_1 = new JMenu("Busqueda");
		menu.add(menu_1);
		
		menu_2 = new JMenu("A\u00F1adir");
		menu.add(menu_2);
		
		menu_3 = new JMenu("Modificar");
		menu.add(menu_3);
		
		menu_4 = new JMenu("Proveedores");
		menuBar.add(menu_4);
		
		menu_5 = new JMenu("Busqueda");
		menu_4.add(menu_5);
		
		menu_6 = new JMenu("A\u00F1adir");
		menu_4.add(menu_6);
		
		menu_7 = new JMenu("Modificar");
		menu_4.add(menu_7);
		
		menu_8 = new JMenu("Servicio bar");
		menuBar.add(menu_8);
		
		menu_9 = new JMenu("Comprar producto");
		menu_8.add(menu_9);
		
		menu_10 = new JMenu("A\u00F1adir Producto");
		menu_8.add(menu_10);
		
		menu_11 = new JMenu("Modificar Producto");
		menu_8.add(menu_11);
		
		menu_12 = new JMenu("Reservas");
		menuBar.add(menu_12);
		
		menu_13 = new JMenu("Solicitar reserva");
		menu_12.add(menu_13);
		
		menu_14 = new JMenu("Ver reserva");
		menu_12.add(menu_14);
		
		menu_15 = new JMenu("Ayuda");
		menuBar.add(menu_15);
		
		menu_16 = new JMenu("abrir ticket");
		menu_15.add(menu_16);
		
		menu_17 = new JMenu("Ver tickets pendientes");
		menu_15.add(menu_17);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		Socio.setSelected(true);
		
		txtNombre = new JTextField();
		txtNombre.setText("Nombre :");
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setText("Apellido 1 :");
		txtApellido.setColumns(10);
		
		txtApellido_1 = new JTextField();
		txtApellido_1.setText("Apellido 2 :");
		txtApellido_1.setColumns(10);
		
		txtDni = new JTextField();
		txtDni.setText("DNI :");
		txtDni.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		
		JSpinner spinner_1 = new JSpinner();
		
		JTextPane txtpnAlmendro = new JTextPane();
		txtpnAlmendro.setBackground(Color.LIGHT_GRAY);
		txtpnAlmendro.setText("Telefono 1");
		
		JTextPane txtpnDireccin = new JTextPane();
		txtpnDireccin.setBackground(Color.LIGHT_GRAY);
		txtpnDireccin.setText("Direcci\u00F3n");
		
		JTextPane txtpnJess = new JTextPane();
		txtpnJess.setBackground(Color.LIGHT_GRAY);
		txtpnJess.setText("Jes\u00FAs");
		
		JTextPane txtpnl = new JTextPane();
		txtpnl.setBackground(Color.LIGHT_GRAY);
		txtpnl.setText("Localidad");
		
		txtEmail = new JTextField();
		txtEmail.setText("Email :");
		txtEmail.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setText("Apellido 1 :");
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setText("Apellido 2 :");
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setText("DNI :");
		textField_3.setColumns(10);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBackground(Color.LIGHT_GRAY);
		textPane_1.setText("655 59 98 78");
		
		JTextPane txtpnCmonasterio = new JTextPane();
		txtpnCmonasterio.setBackground(Color.LIGHT_GRAY);
		txtpnCmonasterio.setText("C:\\Samos");
		
		JTextPane txtpnXxxxxgmailcom = new JTextPane();
		txtpnXxxxxgmailcom.setBackground(Color.LIGHT_GRAY);
		txtpnXxxxxgmailcom.setText("XXXXX@gmail.com");
		
		JTextPane txtpnZaragoza = new JTextPane();
		txtpnZaragoza.setBackground(Color.LIGHT_GRAY);
		txtpnZaragoza.setText("Zaragoza");
		
		JButton btnAceptar = new JButton("Aceptar");
		
		JButton btnCancelar = new JButton("Cancelar");
		
		JButton btnModificar = new JButton("Modificar");
		
		JButton btnNominas = new JButton("Nominas");
		
		passwordField = new JPasswordField();
		passwordField.setText("**********");
		
		JInternalFrame internalFrame = new JInternalFrame("Informaci\u00F3n Adicional del socio");
		
		JTextArea txtrCodigoInvitado = new JTextArea();
		internalFrame.getContentPane().add(txtrCodigoInvitado, BorderLayout.CENTER);
		txtrCodigoInvitado.setText("Codigo de Invitaci\u00F3n: 56876");
		
		JRadioButton rdbtnActivarCodigoInvitacion = new JRadioButton("Activar Codigo Invitacion");
		rdbtnActivarCodigoInvitacion.setEnabled(false);
		internalFrame.getContentPane().add(rdbtnActivarCodigoInvitacion, BorderLayout.SOUTH);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/imagen/descarga.jpg")));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(13)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(btnNominas, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(Invitado, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(99)
									.addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(Trabajador, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(99)
									.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(Socio, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
							.addGap(92)
							.addComponent(btnAceptar, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
					.addGap(62)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(internalFrame, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(24)
							.addComponent(btnCancelar, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
							.addGap(74)
							.addComponent(btnModificar, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(26)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(txtDni, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnl, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(91)
							.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnZaragoza, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnJess, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(91)
							.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnXxxxxgmailcom, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnAlmendro, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(91)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(textPane_1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(txtApellido_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnDireccin, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(91)
							.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(txtpnCmonasterio, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 163, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(34, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnJess, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnXxxxxgmailcom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(13)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnAlmendro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textPane_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(24)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txtApellido_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnDireccin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnCmonasterio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(21)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txtDni, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnl, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtpnZaragoza, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(25))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 173, GroupLayout.PREFERRED_SIZE)
							.addGap(18)))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(20)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNominas)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(1)
									.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
							.addGap(30)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(Invitado, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGap(34)
											.addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(Trabajador, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGap(7)
											.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(27)
									.addComponent(Socio, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(56)
									.addComponent(btnAceptar))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(internalFrame, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
							.addGap(20)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnCancelar)
								.addComponent(btnModificar)))))
		);
		contentPane.setLayout(gl_contentPane);
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txtNombre, Trabajador, Socio, Invitado, txtApellido, txtApellido_1, txtDni, spinner, spinner_1, txtpnAlmendro, txtpnDireccin, txtpnJess, txtpnl, txtEmail, textField_1, textField_2, textField_3, textPane_1, txtpnCmonasterio, txtpnXxxxxgmailcom, txtpnZaragoza, btnAceptar, btnCancelar, btnModificar, btnNominas, passwordField, internalFrame, internalFrame.getContentPane(), txtrCodigoInvitado}));
		internalFrame.setVisible(true);
	}
}
