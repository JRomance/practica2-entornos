﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_Jesus
{
    class Program
    {        
        static void Main(string[] args)
        {
            int opcion = 0,num1 = 0, num2 = 0,c=0;
            String cadena = "";
            Boolean salir = false;
            Console.WriteLine("Bienvenido a la calculadora");
            do
            {
                do
                {
                    Console.WriteLine("Introduce una de las siguientes opciones:");
                    Console.WriteLine("Pulse 1 para leer dos valores enteros ");
                    Console.WriteLine("Pulse 2 para sumar los valores");
                    Console.WriteLine("Pulse 3 para restar los valores");
                    Console.WriteLine("Pulse 4 para multiplicar los valores");
                    Console.WriteLine("Pulse 5 para Salir");
                    cadena = Console.ReadLine();
                    opcion = int.Parse(cadena);
                } while (opcion < 0 || opcion > 5);
                Console.WriteLine("Su opcion elegida es la " + opcion);
                c = 1;
                switch (opcion)
                {
                    case 1:
                        num1 = leerEntero();
                        num2 = leerEntero();
                        break;
                    case 2:
                        sumaNum(num1,num2);
                        break;
                    case 3:
                        restarNum(num1,num2);
                        break;
                    case 4:
                        multiplicarNum(num1, num2);
                        break;
                    case 5:
                        salir = true;
                        break;

                }
               
            } while (salir != true);
        }

        private static void multiplicarNum(int num1,int num2) {
            Console.WriteLine("La suma de los dos valores tiene el valor" + (num1 * num2));
        }
        private static int  leerEntero() {
            int num1 = 0;       
            Console.WriteLine("Introduce un valor entero");
            
            num1=int.Parse(Console.ReadLine());
            return num1;
           

        }
        private static void sumaNum(int num1,int num2) {
            Console.WriteLine("La suma de los dos valores tiene el valor" + (num1 + num2));
        }
        private static void restarNum(int num1, int num2) {
            Console.WriteLine("La resta de los dos valores tiene el valor" + (num1 - num2));
        }
    }
}
