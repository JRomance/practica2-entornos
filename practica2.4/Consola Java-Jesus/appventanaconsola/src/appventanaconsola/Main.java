package appventanaconsola;

import java.util.Scanner;

public class Main {
	static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int opcion = 0, num1 = 0, num2 = 0, c = 0;
		String cadena = "";
		Boolean salir = false;
		System.out.println("Bienvenido a la calculadora");
		do {
			do {
				System.out.println("Introduce una de las siguientes opciones:");
				System.out.println("Pulse 1 para leer dos valores enteros ");
				System.out.println("Pulse 2 para sumar los valores");
				System.out.println("Pulse 3 para restar los valores");
				System.out.println("Pulse 4 para Salir");
				opcion = teclado.nextInt();
			} while (opcion < 0 || opcion > 4);
			System.out.println("Su opcion elegida es la " + opcion);
			c = 1;
			switch (opcion) {
			case 1:
				num1 = leerEntero();
				num2 = leerEntero();
				break;
			case 2:
				sumaNum(num1, num2);
				break;
			case 3:
				restarNum(num1, num2);
				break;
			case 4:
				salir = true;
				break;

			}

		} while (salir != true);
	}

	

	private static int leerEntero() {
		int num1 = 0;
		System.out.println("Introduce un valor entero");

		num1 = teclado.nextInt();
		return num1;

	}

	private static void sumaNum(int num1, int num2) {
		System.out.println("La suma de los dos valores tiene el valor" + (num1 + num2));
	}

	private static void restarNum(int num1, int num2) {
		System.out.println("La resta de los dos valores tiene el valor" + (num1 - num2));
	}

}
