﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class2
    {
        public static Boolean esPrimo(int numero)
        {
            int contadorDivisores = 1;
            for (int i = 1; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    contadorDivisores++;
                }

            }
            Boolean primo = false;
            if (contadorDivisores == 2)
            {
                primo = true;
                return primo;
            }
            else
            {
                primo = false;
                return primo;
            }

        }

        public static int sumar(int numero1, int numero2)
        {
            int resultado = numero1 + numero2;
            return resultado;
        }

        public static int restar(int numero1, int numero2)
        {
            int resultado = numero1 - numero2;
            return resultado;
        }

        public static int factorial(int numero1)
        {
            int resultado = 0;
            for (int i = numero1; i > 0; i--)
            {
                resultado *= i;
            }
            return resultado;
        }

        public static int potencia(int numero1, int exponente)
        {
            int resultado = 0;
            for (int i = 0; i <= exponente; i++)
            {
                resultado *= numero1;

            }
            return resultado;
        }
    }
}
