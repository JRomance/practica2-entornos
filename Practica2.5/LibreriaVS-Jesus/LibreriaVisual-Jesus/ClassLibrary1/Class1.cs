﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class1
    {
        public static int multiplicar(int numero1, int numero2)
        {
            int resultado = numero1 * numero2;
            return resultado;
        }
        public static int dividir(int numero1, int numero2)
        {
            int resultado = numero1 / numero2;
            return resultado;
        }
        public static int resto(int numero1, int numero2)
        {
            int resultado = numero1 % numero2;
            return resultado;
        }
        public static Boolean esPar(int numero1)
        {
            int resultado = numero1;
            Boolean par = false;
            if (numero1 % 2 == 0)
            {
                par = true;
                return par;
            }
            else
            {
                return par;
            }

        }
        public static Boolean esBisiesto(int a)
        {
            if (a % 4 == 0 && a % 100 != 0 || a % 400 == 0)
                return true;
            else
                return false;
        }
    }
}
